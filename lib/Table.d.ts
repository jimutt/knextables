/// <reference types="knex" />
import { DbFieldMap } from '.';
import * as Knex from 'knex';
export default class Table {
    private _tableName;
    private _db;
    private _fields;
    constructor(knexDb: Knex, tableName: string, dbFieldMap: DbFieldMap);
    all(): Promise<any>;
    find(id: string | number): Promise<any>;
    create(row: object): Promise<any>;
    upsert(row: object): Promise<any>;
    update(id: string | number, row: object): Promise<any>;
    delete(id: string | number): Promise<any>;
    query(): Knex.QueryBuilder;
    readonly _table: Knex.QueryBuilder;
}
