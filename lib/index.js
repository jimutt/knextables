"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Table_1 = require("./Table");
exports.Table = Table_1.default;
const DbFieldMap_1 = require("./DbFieldMap");
exports.DbFieldMap = DbFieldMap_1.default;
exports.default = {
    Table: Table_1.default,
    DbFieldMap: DbFieldMap_1.default
};
