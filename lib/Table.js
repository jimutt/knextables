"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
class Table {
    constructor(knexDb, tableName, dbFieldMap) {
        this._tableName = tableName;
        this._db = knexDb;
        this._fields = dbFieldMap;
    }
    all() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._table.select(...this._fields.toSqlAliases());
        });
    }
    find(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._table
                .select(...this._fields.toSqlAliases())
                .limit(1)
                .where('id', id);
        });
    }
    create(row) {
        return __awaiter(this, void 0, void 0, function* () {
            row = this._fields.jsToDbObject(row);
            return yield this._table.insert(row);
        });
    }
    upsert(row) {
        return __awaiter(this, void 0, void 0, function* () {
            const query = this._table.insert(row);
            return yield this._db.raw('? ON CONFLICT DO UPDATE', [query]);
        });
    }
    update(id, row) {
        return __awaiter(this, void 0, void 0, function* () {
            row = this._fields.jsToDbObject(row);
            return yield this._table.where('id', id).update(row);
        });
    }
    delete(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._table.where('id', id).del();
        });
    }
    query() {
        return this._table;
    }
    get _table() {
        return this._db(this._tableName);
    }
}
exports.default = Table;
