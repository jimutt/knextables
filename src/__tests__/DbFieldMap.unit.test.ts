import DbFieldMap from '../DbFieldMap';

describe('constructor', () => {
  test('Set private _fields property to constructor fieldMap parameter.', () => {
    const fields = ['gsagsa', ['asd', 'asdfsa']];
    const map = new DbFieldMap(fields) as any;

    expect(map._fields).toBe(fields);
  });

  test('Throws if fieldMap parameter is object', () => {
    expect(() => {
      new DbFieldMap({ nane: 'hi' } as any);
    }).toThrow();
  });

  test('Throws if fieldMap parameter is string or number', () => {
    expect(() => {
      new DbFieldMap('hi' as any);
    }).toThrow();
    expect(() => {
      new DbFieldMap(12 as any);
    }).toThrow();
  });

  test('Throws if member of fieldMap is an object', () => {
    expect(() => {
      new DbFieldMap([{ name: 'greg' }] as any);
    }).toThrow();
  });

  test('Throws if member of fieldMap is an array and length !== 2', () => {
    expect(() => {
      new DbFieldMap([['field', 'fieldAlias', 'extraString']]);
    }).toThrow();
  });

  test('Throws if a member of fieldMap is an array and an item of the array is an object or array', () => {
    expect(() => {
      new DbFieldMap(['field', ['fieldAlias', ['alias2']]] as any);
    }).toThrow();
    expect(() => {
      new DbFieldMap([['field', { name: 'field' }]] as any);
    }).toThrow();
  });
});

describe('toSqlAliases', () => {
  test('Single field, no alias if not specified', () => {
    const fields = ['noAlias'];
    const map = new DbFieldMap(fields);
    expect(map.toSqlAliases()).toEqual(['noAlias']);
  });

  test('Single field, generate alias', () => {
    const fields = [['noAlias', 'theAlias']];
    const map = new DbFieldMap(fields);
    expect(map.toSqlAliases()).toEqual(['noAlias as theAlias']);
  });

  test('Multiple fields, no aliases', () => {
    const fields = ['noAlias', 'noAlias2'];
    const map = new DbFieldMap(fields);
    expect(map.toSqlAliases()).toEqual(fields);
  });

  test('Multiple fields, generate aliases', () => {
    const fields = [['noAlias', 'theAlias'], ['noAlias2', 'theAlias2']];
    const map = new DbFieldMap(fields);
    expect(map.toSqlAliases()).toEqual([
      'noAlias as theAlias',
      'noAlias2 as theAlias2'
    ]);
  });

  test('Combined alias fields and non-alias fields', () => {
    const fields = ['normalNonAlias', ['noAlias', 'theAlias']];
    const map = new DbFieldMap(fields);
    expect(map.toSqlAliases()).toEqual([
      'normalNonAlias',
      'noAlias as theAlias'
    ]);
  });
});

describe('aliasToField', () => {
  test('Works for fields with no alias', () => {
    const fields = ['normalNonAlias', 'nonAlias2'];
    const map = new DbFieldMap(fields);
    expect(map.aliasToField('nonAlias2')).toBe('nonAlias2');
  });

  test('Works for field with alias', () => {
    const fields = ['normalNonAlias', ['created_at', 'createdAt']];
    const map = new DbFieldMap(fields);
    expect(map.aliasToField('createdAt')).toBe('created_at');
  });

  test('Throws if alias or field does not exist', () => {
    const fields = ['normalNonAlias', ['updated_at', 'updatedAt']];
    const map = new DbFieldMap(fields);
    expect(() => {
      map.aliasToField('createdAt');
    }).toThrow();
  });
});

describe('jsToDbObject', () => {
  test('Works for fields with no alias', () => {
    const fields = ['normalNonAlias', 'nonAlias2'];
    const map = new DbFieldMap(fields);
    const aliasObject = {
      normalNonAlias: 1
    };
    expect(map.jsToDbObject(aliasObject)).toEqual(aliasObject);
  });

  test('Works for fields with aliases', () => {
    const fields = [['updated_at', 'updatedAt']];
    const map = new DbFieldMap(fields);
    const aliasObject = {
      updatedAt: 1
    };
    expect(map.jsToDbObject(aliasObject)).toEqual({ updated_at: 1 });
  });
});
