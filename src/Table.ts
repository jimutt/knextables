import { DbFieldMap } from '.';
import * as Knex from 'knex';

/**
 * Generic database table access object with basic CRUD functionality.
 * @param {Object} db - Knex database instance
 * @param {string} tableName
 * @param {DbFieldMap} dbFieldMap
 */
export default class Table {
  private _tableName: string;
  private _db: Knex;
  private _fields: DbFieldMap;

  constructor(knexDb: Knex, tableName: string, dbFieldMap: DbFieldMap) {
    this._tableName = tableName;
    this._db = knexDb;
    this._fields = dbFieldMap;
  }

  /**
   * Fetch all rows
   */
  async all() {
    return await this._table.select(...this._fields.toSqlAliases());
  }

  /**
   * Get single row by id
   * @param {string|number} id - Value of the table's "id" field.
   */
  async find(id: string | number) {
    return await this._table
      .select(...this._fields.toSqlAliases())
      .limit(1)
      .where('id', id);
  }

  /**
   * Insert single row
   * @param {object} row
   */
  async create(row: object) {
    row = this._fields.jsToDbObject(row);
    return await this._table.insert(row);
  }

  /**
   * Update or insert if not present. NEED TO BE TESTED! May not be working!
   * @param {object} row
   */
  async upsert(row: object) {
    const query = this._table.insert(row);
    return await this._db.raw('? ON CONFLICT DO UPDATE', [query]);
  }

  /**
   * Update existing row.
   * @param {string|number} id - Old row ID
   * @param {object} row - Updated entry
   */
  async update(id: string | number, row: object) {
    row = this._fields.jsToDbObject(row);
    return await this._table.where('id', id).update(row);
  }

  /**
   *
   * @param {string|number} id - Row ID
   */
  async delete(id: string | number) {
    return await this._table.where('id', id).del();
  }

  query() {
    return this._table;
  }

  get _table() {
    return this._db(this._tableName);
  }
}
