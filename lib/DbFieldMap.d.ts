export default class DbFieldMap {
    private _fields;
    constructor(fieldMap: (string | string[])[]);
    toSqlAliases(): string[];
    aliasToField(alias: string): string;
    jsToDbObject(aliasObj: any): any;
}
