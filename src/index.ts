import Table from './Table';
import DbFieldMap from './DbFieldMap';

export default {
  Table,
  DbFieldMap
};

export { Table, DbFieldMap };
