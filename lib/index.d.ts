import Table from './Table';
import DbFieldMap from './DbFieldMap';
declare const _default: {
    Table: typeof Table;
    DbFieldMap: typeof DbFieldMap;
};
export default _default;
export { Table, DbFieldMap };
