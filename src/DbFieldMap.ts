import * as Joi from 'joi';

const fieldMapSchema = Joi.array().items(
  Joi.string(),
  Joi.number(),
  Joi.array()
    .items(Joi.string(), Joi.number())
    .length(2)
);

/**
 * @class DbFieldMap
 */
export default class DbFieldMap {
  private _fields: (string | string[])[];

  constructor(fieldMap: (string | string[])[]) {
    const validationResult = Joi.validate(fieldMap, fieldMapSchema);
    if (validationResult.error) throw validationResult.error;
    this._fields = fieldMap;
  }

  toSqlAliases(): string[] {
    let result = [];
    for (const field of this._fields) {
      if (Array.isArray(field)) result.push(`${field[0]} as ${field[1]}`);
      else result.push(field);
    }
    return result;
  }

  aliasToField(alias: string): string {
    for (const field of this._fields) {
      if (field === alias) return field;
      if (Array.isArray(field) && field[1] === alias) return field[0];
    }
    throw new Error(`No field found for alias ${alias}.`);
  }

  jsToDbObject(aliasObj: any) {
    let result: any = {};
    for (const prop in aliasObj) {
      if (!aliasObj.hasOwnProperty(prop)) continue;
      result[this.aliasToField(prop)] = aliasObj[prop];
    }
    return result;
  }
}
